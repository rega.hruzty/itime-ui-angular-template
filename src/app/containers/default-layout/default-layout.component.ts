import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {

  /* global variables */
  bendera: string;
  name: string = 'User';
  viewIds: string[] = [];

  /* sidebar variables */
  public navItems: any[] = [];
  public sidebarMinimized = true;
  private changes: MutationObserver;

  /* other variables */
  public basePath = environment.basePath;
  public element: HTMLElement = document.body;

  constructor(
    private _router: Router,
    @Inject(DOCUMENT) private docGo: Document) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }

  ngOnInit(): void {
  
  }
}
