export const environment = {
  production: true,
  clientId: 'itime',
  clientSecret: 123456,
  authUri: '/security-management/oauth/token',
  checkToken: '/security-management/oauth/check_token',
  resourceUri: '/resource-management/api',
  excelReaderUri: '/excel-reader/api',
  registrationUri: '/registration/api',
  securityUri: '/security-management/api',
  securityMenu: '/security-management/menu',
  authLogout: '/security-management/security',
  remysUri: '/web-application-face/service',
  alfrescoUri: '/alfresco',
  remysUrlDeploy: 'http://localhost:8000',
  basePath: '/itime/resource-management/',
  siteUrl: '/itime'
};