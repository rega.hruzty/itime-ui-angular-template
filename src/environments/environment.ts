// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  clientId: 'itime',
  clientSecret: 123456,
  authUri: '/security-management/oauth/token',
  checkToken: '/security-management/oauth/check_token',
  resourceUri: '/resource-management/api',
  excelReaderUri: '/excel-reader/api',
  registrationUri: '/registration/api',
  securityUri: '/security-management/api',
  securityMenu: '/security-management/menu',
  authLogout: '/security-management/security',
  remysUri: '/web-application-face/service',
  alfrescoUri: '/alfresco',
  basePath: '/',
  siteUrl: '',
  remysUrlDeploy: 'http://10.243.131.46:8000'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
